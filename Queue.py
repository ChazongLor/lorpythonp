# This file will contain a class that defines the data structure
# priority queue. This will be done by using Python's heapq push method
# which will take in two priorities as well as the item, it will use
# these to order the queue.
#

import heapq

class PriorityQueue:
    def __init__(self):
        self.__queue_l = [] # A list to store the queue
        self.__index = 0    # The index of the item in the queue
    """
    This method will take in an item to be added into the queue, as well as
    a primary priority and a secondary priority, these priorities should be integers. 
    The heappush method is called with the list that will store the queue and then a
    tuple with priority, priority2, the index and the item to be pushed. This method
    will use priority, then priority2 and finally the index to order the item.
    As safety, in case an item has the same values, the index will be the deciding
    factor to see what item goes first.
    The item that has the lowest priority will be popped off first. After that is done
    the index will increment.
    """
    def push(self, item, priority, priority2):
        heapq.heappush(self.__queue_l, (priority, priority2, self.__index, item))
        self.__index += 1
    
    """
    This function uses heapq heappop to return the smallest item in the queue
    It will return the last item in the list.
    """
    def pop(self):
        return heapq.heappop(self.__queue_l)[-1]

    """
    Returns true if the queue is empty, false otherwise.
    It will check to see if the length of the list
    storing the queue is equal to zero
    """
    def empty(self):
        return len(self.__queue_l) == 0
