This is a readme for the Python Project.

The project is about creating an airplane take-off time slot simulation. Where
we need to keep track of the information provided: aircraft identifier, submission time, start time requested, length of time requested, actual start time, actual end time.

Please type in the command line 
-cd and whereever you have put the folder containing these files.
	Example: cd Desktop
then
-cd and the folder name
	Example: cd LorPythonP
and lastly
-python3 Main.py
