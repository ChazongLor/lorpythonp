# This class defines for the data structure Request.
# A Request is used by an airplane for a takeoff time
# on a runway
#

class Airplane:
    
    def __init__(self, reqs_id, submiss_time, req_start, req_duration):
        self._id = str(reqs_id)                 #String that identifies the plane that made the request
        self._sub_time = int(submiss_time)      #The submission time for the request
        self._req_start = int(req_start)        #Time the plane requests
        self._req_duration = int(req_duration)  #The duration that the plane requests
        self._start_time = 0
        self._end_time = 0
        
    """
     This function will override the str method
     to define the string representation of a Request object
     This is the following format: [Airplane ID, X, X, X, X, X]
     Will return a string formatted above
    """
    def __str__(self):
        return "[" + self._id + ", " + str(self._sub_time) + ", " + str(self._req_start) + ", " + \
                str(self._req_duration) + "]"
    
    """
     This will override the repr method to define how a Request object
     is displayed when it is stored in a list of Requests and print out
     the items of the list in a for loop.
     Returns a string that is formatted as described in str comments.
    """
    def __reqr__(self):
        return str(self)
    
    # Returns the integer submission time
    #
    def get_sub_time(self):
        return self._sub_time
    
    # Returns the integer requested start time
    #
    def get_req_start(self):
        return self._req_start
    
    # Returns the string Airplane ID
    #
    def get_id(self):
        return self._id
    
    # Returns the integer requested duration
    #
    def get_req_duration(self):
        return self._req_duration
        
    # Returns the integer actual start time
    #
    def get_actual_start(self):
        return self._start_time
        
    # Returns the integer actual end time
    #
    def get_actual_end(self):
        return self._end_time
    
    # Takes in integer and sets actual start data
    #
    def actual_start(self, start):
        self._start_time = start

    # Takes in integer and sets actual end data
    #
    def actual_end(self, end):
        self._end_time = end