"""
This class uses the Python time module as well as the Airplane and Queue classes that I defined, which is in the
file that you download from BitBucket.

Most of the functionality of the project is in this class. Takes in an input file when an instance of the class is
created. The __run_pro method is the only public method that when called will verify the input file is in the format of:

Delta 160, 0, 0, 4
UAL 120, 0, 5, 4
Delta 6, 2, 3, 6

The parameters are: Request ID, request submission time, requested start time and requested duration.
Additional constraints include: Request ID must be a string containing at least 1 character, the request submission time,
requested start time and the requested duration values must be integers and the time slot requested must be greater than
or equal to the requested submission time. Program will not run and will instead display an error message if invalid input
or file format is passed in.
"""

import Airplane as Airplane
import Queue as Queue
import time

class ProcessRequest:
    def __init__(self, file_name):
        self.__input_list = []
        self.__file_name = file_name
    
    """
    This method should verify that the input file ends with the file extenstion .txt. It then parses the input
    file by splitting by line and then split each line as a delimiter and removing all the white space. It will then
    push the list of lists to validate. If validate returns True, it will create Request objects for each line of the
    input file and stores them in the __input_list data attribute. Then it will sort this list by submision time as first priority 
    and requested start time as the second priority. How it does this is by putting these items in a PriorityQueue and then 
    popping them off back into __input_list.
    
    If the processing is successful, function returns true, otherwise false.
    """
    def __input_process(self):
        if self.__file_name.endswith('.txt'):
            with open(self.__file_name) as file_in:
                entire_file = file_in.read().splitlines()
                for i in range(len(entire_file)):
                    entire_file[i] = entire_file[i].split(',')
                    for j in range(len(entire_file[i])):
                        entire_file[i][j] = entire_file[i][j].strip()
            if self.__validate(entire_file):
                for item in entire_file:
                    self.__input_list.append(Airplane.Airplane(item[0], int(item[1]), int(item[2]), int(item[3])))
                qu = Queue.PriorityQueue()
                for i in self.__input_list:
                    qu.push(i, i.get_sub_time(), i.get_req_start())
                for i in range(len(self.__input_list)):
                    self.__input_list[i] = qu.pop()
                return True
            else:
                return False
        else:
            print("Please input a txt file")
            return False
    
    """
    This method takes in a list of a list of strings. It verifies that the format matches our above input format. 
    It ensures there are 4 values on each line, that the first value is a string and 
    the second, third and fourth are integers.
    
    This function returns true if the input list is in the correct format, false otherwise.
    """
    @staticmethod
    def __validate(input_data):
        for item in input_data:
            if len(item) != 4:
                print(item)
                print("Please ensure the file has 4 values on each line")
                valid_input = False
                break
            if item[0].isdigit():
                print(item)
                print("Please ensure the first item of each line is a string")
                valid_input = False
                break
            try:
                item[1] = int(item[1])
            except ValueError:
                print(item)
                print("Please ensure the second item of each line is an integer")
                valid_input = False
                break
            try:
                item[2] = int(item[2])
            except ValueError:
                print(item)
                print("Please ensure the third item of each line is an integer")
                valid_input = False
                break
            if item[1] > item[2]:
                print(item)
                print("Request time must be >= submission time, cannot request a time that has already passed")
                valid_input = False
                break
            try:
                item[3] = int(item[3])
            except ValueError:
                print(item)
                print("Please ensure the fourth item of each line is an integer")
                valid_input = False
                break
            valid_input = True
        return valid_input
    
    
    """
    This method takes in a list of Request objects.
    
    Every Request object will have additional data attribute for the actual_start and actual_end which are 0 upon
    creation. Will calculate what the values should be based on the object's position on the list. It does this by
    setting the first item in the list's actual start to the max of the actual start currently stored in the object
    or the requested start. This will allow the queue to have empty spots.
    
    For all the other items in the list, the actual start and requested duration for the previous Request item are 
    added together, this then becomes the actual start time (or scheduled start time) for the current item. This is 
    the earliest the plane may take off, if the plane gets to the front of the queue but has a requested start time 
    later than the current time then this will be handled with the max described above.
    
    The list of Request objects with the data attribute actual_start and actual_end will be returned
    """
    @staticmethod
    def __cal_time(a_list):
        if len(a_list) > 0:
            a_list[0].actual_start(max(int(a_list[0].get_actual_start()), int(a_list[0].get_req_start())))
            for i in range(1, len(a_list)):
                offset = a_list[i - 1].get_actual_start() + a_list[i - 1].get_req_duration()
                if offset > a_list[i].get_req_start():
                    a_list[i].actual_start(offset)
                else:
                    a_list[i].actual_start(a_list[i].get_actual_start())
            for i in range(len(a_list)):
                a_list[i].actual_end(a_list[i].get_actual_start() + a_list[i].get_req_duration())
        return a_list
    
    """
    This method takes in a list of Request objects and the current time. 
    It checkes to see if the length of the current_list that is passed in is greater than 0. 
    It then checks to see if the first item in the list's actual end time is <= the current time.
    
    If the conditions are true, the first item in the list is removed and the rest of the list is returned.
    Otherwise, the list is returned unchanged.
    """
    @staticmethod
    def __remove_checker(current_list, timer):
        if len(current_list) > 0 and current_list[0].get_actual_end() <= timer:
            current_list.remove(current_list[0])
        return current_list
    
    """
    This method takes in a list of Request objects. It sorts the list using a Priority Queue. The requested start time is used as the first priority
    and the submision time is the second priority with the position in the list as the tie breaker. The items are then pushed onto the queue and 
    then popped off and put back into the list in a new order.

    A sorted list of Request objects are returned.
    """
    @staticmethod
    def __sort_by_req_time(a_list):
        q = Queue.PriorityQueue()
        for i in a_list:
            q.push(i, i.get_req_start(), i.get_sub_time())
        for i in range(len(a_list)):
            a_list[i] = q.pop()
        return a_list
        
    """
    The method takes in a list of Request objects and the current time.
    
    The output for this should be in the format:
    
    At time X the queue would look like: Request ID (scheduled for/started at X)
    
    An example would be:
    At time 0 the queue would look like: Delta 160 (started at 0), UAL 120 (scheduled for 5)
    At time 1 the queue would look like: Delta 160 (started at 0), UAL 120 (scheduled for 5)
    At time 2 the queue would look like: Delta 160 (started at 0), Delta 6 (scheduled for 4), UAL 120 (scheduled for 10)
    At time 3 the queue would look like: Delta 160 (started at 0), Delta 6 (scheduled for 4), UAL 120 (scheduled for 10)
    At time 4 the queue would look like: Delta 6 (started at 4), UAL 120 (scheduled for 10)
    
    At time X the queue would look like: EMPTY
    
    This method returns a string formatted like above.
    """
    @staticmethod
    def __output_format(a_list, timer):
        retrn_val = "At time " + str(timer) + " the queue would like: "
        if len(a_list) > 0:
            if len(a_list) == 1:
                if a_list[0].get_actual_start() <= timer:
                    retrn_val += a_list[0].get_id() + " (started at " + str(a_list[0].get_actual_start()) + ")"
                else:
                    retrn_val += a_list[0].get_id() + " (scheduled for " + str(a_list[0].get_actual_start()) + ")"
                return retrn_val
            else:
                if a_list[0].get_actual_start() <= timer:
                    retrn_val += a_list[0].get_id() + " (started at " + str(a_list[0].get_actual_start()) + "), "
                else:
                    retrn_val += a_list[0].get_id() + " (scheduled for " + str(a_list[0].get_actual_start()) + "), "
            for i in range(1, len(a_list)-1):
                 retrn_val += a_list[i].get_id() + " (scheduled for " + str(a_list[i].get_actual_start()) + "), "
            retrn_val += a_list[len(a_list) - 1].get_id() + " (scheduled for " + str(a_list[len(a_list) - 1].get_actual_start()) + ")"
        else:
            retrn_val += "EMPTY"
        return retrn_val
        
    """
    This method takes in a list of Request objects. The program should print out the request ids and the actual start and end times
    of each plane at the end of program.
    The output should be in this format:

    Request ID (X-X), Request ID (X-X), Request ID (X-X)

    Example would be:
    Delta 160 (0-3), Delta 6 (4-9), UAL 120 (10-13)

    The method will return a string formatted as described. If an empt input was passed then
    it will return the string "Empty input was passed in."
    """
    @staticmethod
    def __final_print(a_list):
        retrn_val = ""
        if len(a_list) > 0:
            for i in range(len(a_list)-1):
                retrn_val += a_list[i].get_id() + " (" + str(a_list[i].get_actual_start()) + "-" + str(a_list[i].get_actual_end()) + "), "
            retrn_val += a_list[len(a_list)-1].get_id() + " (" + str(a_list[len(a_list)-1].get_actual_start()) + "-" + str(a_list[len(a_list)-1].get_actual_end()) + ")"
        else:
            retrn_val += "Empty input has been passed in."
        return retrn_val
    
    """
    Run is the only public method in this class and it was drives the program. It uses many of the private methods of
    the class. First, process input is called to populate input_list. A current and final list are
    created and the timer is set to 0. The method enters a loop which continually does the following: checks to see if
    any of the requests are finished and needs to be removed, checks to see if any new requests have been submitted,
    sorts the requests, calculates the actual start and end times, check to see if all requests have been processed (and
    breaks out of the loop if they have), prints out the current status of the queue, sleeps for one second and then
    increments time. The final output is printed out after being processed.
    """
    def __run_pro(self):
        if self.__input_process():
            current_list = []
            final_list = []
            timer = 0
            while True:
                current_list = self.__remove_checker(current_list, timer)
                for i in self.__input_list:
                    if i.get_sub_time() == timer:
                        current_list.append(i)
                        final_list.append(i)
                current_list = self.__sort_by_req_time(current_list)
                current_list = self.__cal_time(current_list)
                if len(current_list) == 0 and len(final_list) == len(self.__input_list):
                    break
                print(self.__output_format(current_list, timer))
                time.sleep(1)
                timer += 1
            final_list = self.__sort_by_req_time(final_list)
            print(self.__final_print(final_list))
